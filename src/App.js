// import React, { Component } from 'react';
// import CanvasJSReact from './canvasjs.react';
// import data from './data/data.json'
// import { directive } from '@babel/types';
// var CanvasJSChart = CanvasJSReact.CanvasJSChart;

 
// class App extends Component {	
//   render() {

    
    
//   const values =   <div>  {data.map((dataDetails, index)=>{
//     console.log(dataDetails) 
//     console.log(JSON.stringify(dataDetails)) 
//     return(
//         <div> {dataDetails.title}</div> 
        
//         )
//       })}  </div>
    
//       values.PropTypes = {
//         dataDetails: React.PropTypes.string,
//       };
//     const options = {
//       title: {
//         text: "Basic Column Chart in React"
//       },
//       data: [{		
//         type: "column",
//                 dataPoints: [
//                     { label: <div>{JSON.parse(JSON.stringify(values))}</div>,  y: 10  },
//                     { label: "Orange", y: 15  },
//                     { label: "Banana", y: 25  },
//                     { label: "Mango",  y: 30  },
//                     { label: "Grape",  y: 28  }
//                 ]
//        }]
//    }

  
		
//    return (
//       <div>
//         <CanvasJSChart options = {options}
//             /* onRef = {ref => this.chart = ref} */
//         />
//       </div>
//     );
//    }}



  

//   export default App;
 
  import React, { Component } from 'react';
import CanvasJSReact from './canvasjs.react';
import { directive } from '@babel/types';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

var dataPoints =[];
class App extends Component {
 
	render() {	
		const options = {
			theme: "light2",
			title: {
				text: "Stock Price of NIFTY 50"
			},
			axisY: {
				title: "Price in USD",
				prefix: "$",
				includeZero: false
			},
			data: [{
				type: "line",
				xValueFormatString: "MMM YYYY",
				yValueFormatString: "$#,##0.00",
				dataPoints: dataPoints
			}]
		}
		return (
		<div>
			<CanvasJSChart options = {options} 
				 onRef={ref => this.chart = ref}
			/>
			{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
		</div>
		);
	}
	
	componentDidMount(){
		var chart = this.chart;
		fetch(`./dataStock.json`)
		.then(function(response) {
			return response.json();
		})
		.then(function(data) {
			for (var i = 0; i < data.length; i++) {
				dataPoints.push({
					x: new Date(data[i].x),
					y: data[i].y
				});
			}
			chart.render();
		});
	}
}
 
  

  export default App;
 